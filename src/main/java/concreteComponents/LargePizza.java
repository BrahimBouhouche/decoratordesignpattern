package concreteComponents;

import component.Pizza;

public class LargePizza extends Pizza {

	
	
	public LargePizza() {
		super();
		Description = "Large Pizza";
	}

	@Override
	public String GetDescription() {

		return Description;
	}

	@Override
	public double CalculateCost() {

		return 10.00;
	}

}
