package concreteComponents;

import component.Pizza;

public class SmallPizza extends Pizza {

	
	
	
	public SmallPizza() {
		super();
		Description = "Small Pizza";
	}

	@Override
	public String GetDescription() {
		// TODO Auto-generated method stub
		return Description;
	}

	@Override
	public double CalculateCost() {
		// TODO Auto-generated method stub
		return 3.00;
	}

}
