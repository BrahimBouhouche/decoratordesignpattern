package concreteComponents;

import component.Pizza;

public class MediumPizza extends Pizza {

	
	
	public MediumPizza() {
		super();
		Description = "Medium Pizza";
		}

	@Override
	public String GetDescription() {
		return Description;
	}

	@Override
	public double CalculateCost() {
		// TODO Auto-generated method stub
		return 5.00;
	}

}
