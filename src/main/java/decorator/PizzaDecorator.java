package decorator;

import component.Pizza;

public class PizzaDecorator extends Pizza {

	// This is the object that will be decoratored
	protected Pizza pizza;

	// Constructor that takes a pizza as parameter

	public PizzaDecorator(Pizza pizza) {
		super();
		this.pizza = pizza;
	}

	@Override
	public double CalculateCost() {
		return  pizza.CalculateCost(); 
	}

	@Override
	public String GetDescription() {

		return pizza.Description;
	}

}
