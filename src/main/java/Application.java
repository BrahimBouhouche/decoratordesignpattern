import component.Pizza;
import concreteComponents.LargePizza;
import concreteDecorator.Cheese;
import concreteDecorator.Ham;
import concreteDecorator.Peppers;

public class Application {

	public static void main(String[] args) {

		Pizza large = new LargePizza();
		
		large = new Cheese(large);
		
		large = new Peppers(large);
		
		System.out.println(large.GetDescription());
		System.out.println(large.CalculateCost());
		
		
	}

}
