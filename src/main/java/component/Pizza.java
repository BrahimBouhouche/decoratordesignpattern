package component;

public abstract class Pizza {
	
	public String Description;

	
	
	public abstract String GetDescription();
	public abstract double CalculateCost();
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return Description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.Description = description;
	}
	
	
	
	
}
