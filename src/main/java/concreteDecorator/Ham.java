package concreteDecorator;

import component.Pizza;
import decorator.PizzaDecorator;

public class Ham extends PizzaDecorator {

	public Ham(Pizza pizza) {
		super(pizza);
		Description = ", Ham";
	}

	@Override
	public String GetDescription() {
		return pizza.GetDescription() + Description;
	}
	
	@Override
	public double CalculateCost() {
		return pizza.CalculateCost() + 5.12;
	}
	
}
