package concreteDecorator;

import component.Pizza;
import decorator.PizzaDecorator;

public class Peppers extends PizzaDecorator {

	public Peppers(Pizza pizza) {
		super(pizza);
		Description = ", peppers.";
	}
	
	@Override
	public String GetDescription() {
		return pizza.GetDescription() + Description;
	}
	
	@Override
	public double CalculateCost() {
		return 	pizza.CalculateCost() + 2.4;
	}

}
