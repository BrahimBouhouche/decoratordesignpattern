package concreteDecorator;

import component.Pizza;
import decorator.PizzaDecorator;

public class Cheese extends PizzaDecorator {

	public Cheese(Pizza pizza) {
		super(pizza);
		
		Description = ", Cheese ";
	}
	
	@Override
	public String GetDescription() {
		return pizza.GetDescription() + Description;
	}
	
	@Override
	public double CalculateCost() {
		return pizza.CalculateCost() + 4.45;
	}

}
